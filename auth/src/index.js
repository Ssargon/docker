const express = require('express');
const { connectDb } = require('./helpers/db');
const { port, host, db } = require('./configuration/index');

const app = express();

const startServer = () => {
    app.listen(port, () => {
        console.log('Started auth servise');
        console.log(`Host: ${host}`);
        console.log(`DB: ${db}`);
    });
};

app.get('/', (req, res) => {
    res.send('Auth Service');
});

connectDb()
    .on('error', console.log)
    .on('disconnected', connectDb)
    .once('open', startServer);