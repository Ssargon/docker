const express = require('express');
const { connectDb } = require('./helpers/db');
const { port, host, db } = require('./configuration/index');
const mongoose = require('mongoose');

const app = express();

const blogSchema = new mongoose.Schema({
    name: String,
});
const Post = mongoose.model("Post", blogSchema);

const startServer = () => {
    app.listen(port, () => {
        console.log('Started api servise');
        console.log(`Host: ${host}`);
        console.log(`DB: ${db}`);

        const post = new Post({ name: "Post" });
        post.save(function(err, savedPost) {
            if (err) return console.log(err);
            console.log('savedPost', savedPost);
        });
    });
};

app.get('/', (req, res) => {
    res.send('Api Service');
});

connectDb()
    .on('error', console.log)
    .on('disconnected', connectDb)
    .once('open', startServer);